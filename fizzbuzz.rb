#! /bin/ruby

class Fizzbuzz
 	def self.generate
 		(1..100).map do |number|
 			fizzbuzz_string = ""

 			fizzbuzz_string += "Fizz" if is_fizz?(number)
 			fizzbuzz_string += "Buzz" if is_buzz?(number)

 			if not fizzbuzz_string.empty?
 				fizzbuzz_string
 			else
 				number.to_s
 			end
	 	end.join("\r\n")
 	end

 	def self.is_fizz?(number)
 		number_is_divisible_by? number, 3
 	end

 	def self.is_buzz?(number)
 		number_is_divisible_by? number, 5
 	end

	def self.number_is_divisible_by?(number, divisor)
		number.modulo(divisor) == 0
	end
 	
 	def self.run
 		puts self.generate
 	end

end

Fizzbuzz.run