require "./fizzbuzz.rb"

RSpec.describe Fizzbuzz do
	def generated_lines
		Fizzbuzz.generate.split("\r\n")
	end

	it "should say 1 as first output" do
		expect(generated_lines.first).to eql '1'
	end

	it "should say 2 as second output" do
		expect(generated_lines[1]).to eql '2'
	end

	describe "Fizzes" do
		fizzes = [3,6,33,99]
		fizzes.each do |fizz|
			it "should say Fizz instead of #{fizz}" do
				expect(generated_lines[fizz-1]).to eql 'Fizz'
			end
		end	

		it "should NOT say just Fizz on a multiple of 3 which is also a multiple of 5" do
			expect(generated_lines[14]).to_not eql 'Fizz'
		end
	end

	describe "Buzzes" do
		buzzes = [5,10,25,100]
		buzzes.each do |buzz|
			it "should say Buzz instead #{buzz}" do
				expect(generated_lines[buzz-1]).to eql 'Buzz'
			end
		end

		it "should NOT say just Buzz on a multiple of 5, which is also a multiple of 3" do
			expect(generated_lines[29]).to_not eql 'Buzz'
		end
	end

	describe "FizzBuzz" do
		it "should say FizzBuzz on every multiple of 3 AND 5" do
			expect(generated_lines[14]).to eql 'FizzBuzz'
		end
	end

	it "should have 100 lines" do
		expect(generated_lines.length).to eql 100
	end
end